package com.ebrd.recruitment.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class UpdateController {


    @Autowired
    EntityUpdater updater;

    private long currentUpdateValue;

    @GetMapping(path = "/{update}/{updateValue}")
    public ResponseEntity<String> update(@PathVariable String update, @PathVariable long updateValue){
        currentUpdateValue = updateValue;
        UpdateEntity entity = new UpdateEntity();
        entity.entityName = update;
        entity.entityValue=updateValue;

        Future response = updater.processUpdate(entity, currentUpdateValue);
        try {
            return new ResponseEntity(response.get(), HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

}
