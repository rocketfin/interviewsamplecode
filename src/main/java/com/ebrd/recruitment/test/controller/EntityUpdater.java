package com.ebrd.recruitment.test.controller;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

@Component
public class EntityUpdater {
    public Map processMap = new HashMap();


    public Future<String> processUpdate(UpdateEntity update, long updateValue) {
        processMap.put(update, updateValue);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        return executor.submit(() -> {
            if (((long) processMap.get(update)) % 16 == 0) {
                double returnValue = updateValue * Math.PI;

                return Double.toString(returnValue);
            } else {
                return update.toString();
            }
        });


    }
}
