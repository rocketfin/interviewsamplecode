package com.ebrd.recruitment.test.controller;

public class UpdateEntity {
    String entityName;
    long entityValue;

    @Override
    public boolean equals(Object o){
        if(o instanceof UpdateEntity && ((UpdateEntity)o).entityName==entityName && ((UpdateEntity)o).entityValue == entityValue){
            return true;
        }
        else return false;
    }
}
